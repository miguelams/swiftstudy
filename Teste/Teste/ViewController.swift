//
//  ViewController.swift
//  Teste
//
//  Created by Miguel Santos on 16/01/2020.
//  Copyright © 2020 Miguel Santos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt1: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl1.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btt(_ sender: Any) {
        if txt1.text!.isEmpty || txt2.text!.isEmpty {
            lbl1.text = "Erro"
        } else {
        let a = Double(txt1.text!)
        let b = Double(txt2.text!)
        let sum = Double(a! + b!)
        lbl1.isHidden = false
        lbl1.text = "Resultado: \(sum)"
        }
    }
}

